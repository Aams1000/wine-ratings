# Created by Andrew

FILE_NAME = "../logs/mlProject.log"

def info(string):
    with open(FILE_NAME, 'a') as log:
        log.write("INFO:   " + str(string) + '\n')
        print(string)

def error(string):
    with open(FILE_NAME, 'a') as log:
            log.write("ERROR:   " + str(string) + '\n')
            print(string)
