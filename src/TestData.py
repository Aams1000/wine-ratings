# Created by Andrew

from sklearn.model_selection import train_test_split
import numpy as np
import Logging

class TestData(object):
    data = None
    algorithmTypes = None
    xName = None
    yName = None
    yIndex = None
    kernels = None
    inputs = None
    labels = None
    xTrain = None
    xTest = None
    yTrain = None
    yTest = None
    shouldPlot = False
    skipColumns = None
    accuracyWindow = None

    def __init__(self, data, algorithmTypes, xName, yName, yIndex, kernels, shouldPlot, skipColumns, accuracyWindow):
        self.data = data 
        self.algorithmTypes = algorithmTypes 
        self.xName = xName 
        self.yName = yName 
        self.yIndex = yIndex 
        self.kernels = kernels 
        self.shouldPlot = shouldPlot
        self.skipColumns = skipColumns
        self.accuracyWindow = accuracyWindow
        self.inputs, self.labels, self.xTrain, self.xTest, self.yTrain, self.yTest = self.prepareInputAndLabels(data, yIndex, skipColumns)

    def prepareInputAndLabels(self, data, yIndex, skipColumns):
        labels = data[:,yIndex]
        skipColumns.append(yIndex)
        inputs = np.delete(data, skipColumns, axis=1)
        # Logging.info("Input shape: " + str(inputs.shape))
        # Logging.info("Labels shape: " + str(labels.shape))

        xTrain, xTest, yTrain, yTest = train_test_split(inputs, labels, test_size=0.25, random_state=0)
        # For single-column inputs, we need to do this hacky reshaping
        if data.shape[1] == 2:
            xTrain = xTrain.reshape(-1, 1)
            yTrain = yTrain.reshape(-1, 1)
            xTest = xTest.reshape(-1, 1)
            yTest = yTest.reshape(-1, 1)
        return inputs, labels, xTrain, xTest, yTrain, yTest





