# Created by Andrew

import numpy as np

ACCURACY_WINDOW = 1

def readInData():
	return np.loadtxt(open("/Users/Andrew/Documents/GraduateSchool/Stanford/2018/AutumnQuarter/CS221/Project/regionOne.txt", "rb"), delimiter="\n", dtype=object)

def main():
	countries = readInData()
	# print(countries.shape)
	code = 0
	codeToCountry = {}
	countryToCode = {}
	newCountries = []
	print(countries.shape)
	for country in countries:
		if country in countryToCode:
			newCountries.append(countryToCode[country])
		else:
			countryToCode[country] = code
			newCountries.append(code)
			code += 1
	print("\n".join(str(code) for code in newCountries))
	# print(len(countries))
	print(countryToCode)
if __name__ == '__main__':
	main()