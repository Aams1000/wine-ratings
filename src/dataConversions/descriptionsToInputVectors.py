import json
import numpy as np
import re
import collections

INPUT_FILE_PATH = "../../data/descriptions.txt"
INPUT_FILE_PATH_POINTS = "../../data/points.txt"
INPUT_FILE_PATH_PRICE = "../../data/prices.txt"

DICT_DUMP_FILE = "../../data/wordsByIndex.json"
MAX_DESCRIPTIONS = 10000
MIN_WORD_FREQUENCY = 5

def readInFile(path, stripPunctuation):
    descriptions = []
    with open(path) as file:
        descriptions = file.readlines()
        # remove header
        descriptions.pop(0)
    descriptions = [re.sub(r'[^\w\s]', '', line).strip('\n') for line in descriptions] if stripPunctuation else descriptions
    return descriptions[:MAX_DESCRIPTIONS]


def generateWordToIndexMap(descriptions):
    wordToIndexMap = {}
    wordIndex = 0
    wordCount = collections.defaultdict(int)
    for description in descriptions:
        for word in description.split(' '):
            wordCount[word] += 1
            if wordCount[word] == MIN_WORD_FREQUENCY:
                wordToIndexMap[word] = wordIndex
                wordIndex += 1
    # print(len(wordToIndexMap))
    return wordToIndexMap


def substituteIndexForWords(description, wordToIndexMap):
    newDescription = np.zeros(len(wordToIndexMap))
    for word in description.split(' '):
        if word in wordToIndexMap:
            newDescription[wordToIndexMap[word]] += 1
    return newDescription


def convertDescriptionsToFeatureVectors(descriptions, wordToIndexMap):
    return [substituteIndexForWords(description, wordToIndexMap) for description in descriptions]


def printDescriptionsWithPoints(points, prices, descriptions):
    # print(len(points), len(descriptions))
    for index, description in enumerate(descriptions):
        print(str(points[index]).strip('\n') + ',' + str(prices[index]).strip('\n') + ',' + ','.join(str(index) for index in description))


def dumpWordToIndexMapToFile(wordToIndexMap):
    with open(DICT_DUMP_FILE, 'w+') as file:
        file.write(json.dumps(wordToIndexMap))

if __name__ == "__main__":
    print("rating,description")
    strippedDescriptions = readInFile(INPUT_FILE_PATH, True)
    wordToIndexMap = generateWordToIndexMap(strippedDescriptions)
    dumpWordToIndexMapToFile(wordToIndexMap)
    descriptionsAsFeatureVectors = convertDescriptionsToFeatureVectors(strippedDescriptions, wordToIndexMap)
    printDescriptionsWithPoints(readInFile(INPUT_FILE_PATH_POINTS, True), readInFile(INPUT_FILE_PATH_PRICE, False), descriptionsAsFeatureVectors)
