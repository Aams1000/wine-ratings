import json
import numpy as np
import re
import collections

INPUT_FILE_PATH = "../../data/descriptions.txt"
INPUT_FILE_PATH_POINTS = "../../data/points.txt"
INPUT_FILE_PATH_PRICE = "../../data/prices.txt"

DICT_DUMP_FILE = "../../data/wordsByIndex.json"
MAX_DESCRIPTIONS = 10000
MIN_WORD_FREQUENCY = 5

def readInFile(path, stripPunctuation):
    descriptions = []
    with open(path) as file:
        descriptions = file.readlines()
        # remove header
        descriptions.pop(0)
    descriptions = [re.sub(r'[^\w\s]', '', line).strip('\n') for line in descriptions] if stripPunctuation else descriptions
    return descriptions[:MAX_DESCRIPTIONS]


def generatewordTupleToIndexMap(descriptions):
    wordTupleToIndexMap = {}
    wordIndex = 0
    wordCount = collections.defaultdict(int)
    for description in descriptions:
        wordList = description.split(' ')
        for index, word in enumerate(wordList):
            if index + 1 < len(wordList):
                wordTuple = (word, wordList[index + 1])
                wordCount[wordTuple] += 1
                if wordCount[wordTuple] == MIN_WORD_FREQUENCY:
                    wordTupleToIndexMap[wordTuple] = wordIndex
                    wordIndex += 1
    # print(len(wordTupleToIndexMap))
    return wordTupleToIndexMap


def substituteIndexForWordTuples(description, wordTupleToIndexMap):
    newDescription = np.zeros(len(wordTupleToIndexMap))
    wordList = description.split(' ')
    for index, word in enumerate(wordList):
        if index + 1 < len(wordList):
            currTuple = (word, wordList[index + 1])
            if currTuple in wordTupleToIndexMap:
                newDescription[wordTupleToIndexMap[currTuple]] += 1
    return newDescription


def convertDescriptionsToFeatureVectors(descriptions, wordTupleToIndexMap):
    return [substituteIndexForWordTuples(description, wordTupleToIndexMap) for description in descriptions]


def printDescriptionsWithPoints(points, prices, descriptions):
    # print(len(points), len(descriptions))
    for index, description in enumerate(descriptions):
        print(str(points[index]).strip('\n') + ',' + str(prices[index]).strip('\n') + ',' + ','.join(str(index) for index in description))


def dumpWordTupleToIndexMapToFile(wordTupleToIndexMap):
    updatedMap = {}
    for key, value in wordTupleToIndexMap.items():
        updatedMap[str(key)] = value

    with open(DICT_DUMP_FILE, 'w+') as file:
        file.write(json.dumps(updatedMap))

if __name__ == "__main__":
    print("rating,description")
    strippedDescriptions = readInFile(INPUT_FILE_PATH, True)
    wordTupleToIndexMap = generatewordTupleToIndexMap(strippedDescriptions)
    dumpWordTupleToIndexMapToFile(wordTupleToIndexMap)
    descriptionsAsFeatureVectors = convertDescriptionsToFeatureVectors(strippedDescriptions, wordTupleToIndexMap)
    printDescriptionsWithPoints(readInFile(INPUT_FILE_PATH_POINTS, True), readInFile(INPUT_FILE_PATH_PRICE, False), descriptionsAsFeatureVectors)
