# Created by Andrew
import operator
import numpy as np
import Plotting, Logging
from sklearn.metrics import confusion_matrix

ACCURACY_WINDOW = 1
WEIGHTS_TO_REPORT = 50

class Course:
    CS221 = "cs221"
    CS229 = "cs229"


class TestType:
    TRAIN = "Train"
    TEST = "Test"


def calculateAccuracy(predictions, labels, window):
    numCorrect = 0
    totalPredictions = len(predictions)
    for index, prediction in enumerate(predictions):
        label = labels[index]
        if label >= (prediction - window) and label <= (prediction + window):
            numCorrect += 1
    accuracy = (numCorrect / float(totalPredictions))
    Logging.info("Classifier predicted " + str(numCorrect) + " out of " + str(totalPredictions) + " correct for " + str(accuracy) + " accuracy")
    # Logging.info("Accuracy: " + str(accuracy))
    return numCorrect, totalPredictions, accuracy


def evaluatePerformance(predictions, y, classifierName, testType, accuracyWindow):
    Logging.info(testType + " results for " + classifierName + ":")
    numCorrect, totalPredictions, accuracy = calculateAccuracy(predictions, y, accuracyWindow)
    return accuracy


def evaluateAndPlotClassifier(classifier, inputs, labels, testType, classifierName, xName, yName, shouldPlot, accuracyWindow, hyperparameters):
    predictions = classifier.predict(inputs)
    accuracy = evaluatePerformance(predictions, labels, classifierName, testType, accuracyWindow)
    plotInfo = Plotting.Info(classifierName, testType, xName, yName, hyperparameters)
    generateAndPlotConfusionMatrices(labels, predictions, plotInfo, accuracy)
    if shouldPlot:
        Plotting.plotAndSaveClassifier(inputs, labels, predictions, accuracy, plotInfo)


def generateAndPlotConfusionMatrices(labels, predictions, plotInfo, accuracy):
    try:
        confusionMatrix = confusion_matrix(labels, predictions)
        labelCategories = set()
        for label in labels:
            labelCategories.add(label)
        Plotting.plotConfusionMatrix(confusionMatrix, list(labelCategories), True, plotInfo, accuracy)
        Plotting.plotConfusionMatrix(confusionMatrix, list(labelCategories), False, plotInfo, accuracy)
    except Exception as e:
        Logging.info("Failed to plot confusion matrix due to exception:")
        Logging.info(e)
    # normalizedMatrix = confusionMatrix.astype('float') / confusionMatrix.sum(axis=1)[:, np.newaxis]
    # Logging.info("Confusion matrix: ")
    # Logging.info(confusionMatrix)
    # Logging.info(normalizedMatrix)


def trainAndTestClassifier(classifier, classifierName, data, shouldPlot, hyperparameters):
    # Logging.info("Before fit")
    classifier.fit(data.xTrain, data.yTrain)
    try:
        if classifier.coef_ is not None:
            computeAndLogTopWeights(classifier.coef_)
    except Exception as e:
        Logging.info("Weights not supported for " + classifierName + " or for current kernel.")
    # Logging.info("after fit")
    evaluateAndPlotClassifier(classifier, data.xTrain, data.yTrain, TestType.TRAIN, classifierName, data.xName, data.yName, shouldPlot, data.accuracyWindow, hyperparameters)
    evaluateAndPlotClassifier(classifier, data.xTest, data.yTest, TestType.TEST, classifierName, data.xName, data.yName, shouldPlot, data.accuracyWindow, hyperparameters)


def computeAndLogTopWeights(weights):
    weightsByIndex = []
    for index, weight in enumerate(weights):
        weightToAppend = weight[index] if weight.shape != () else weight
        weightsByIndex.append((index, weightToAppend))

    weightsByIndex.sort(key=operator.itemgetter(1))
    Logging.info("Top " + str(WEIGHTS_TO_REPORT) + " weights:")
    Logging.info(weightsByIndex[:WEIGHTS_TO_REPORT])
    weightsByIndex.sort(key=operator.itemgetter(1), reverse=True)
    Logging.info("Bottom " + str(WEIGHTS_TO_REPORT) + " weights:")
    Logging.info(weightsByIndex[:WEIGHTS_TO_REPORT])


class Data(object):
    xTrain = None
    yTrain = None
    xTest = None
    yTest = None
    xName = None
    yName = None
    accuracyWindow = None

    def __init__(self, xTrain, xTest, yTrain, yTest, xName, yName, accuracyWindow):
        self.xTrain = xTrain
        self.yTrain = yTrain
        self.xTest = xTest
        self.yTest = yTest
        self.xName = xName
        self.yName = yName
        self.accuracyWindow = accuracyWindow






