# Created by Andrew

import json
import numpy as np
from TestData import TestData

PATH_PREFIX = "../data/"

class JsonDataFields:
    ALGORITHM_TYPES = "algorithmType"
    X_NAME = "xName"
    Y_NAME = "yName"
    Y_INDEX = "yIndex"
    KERNELS = "kernels"
    PATH = "path"
    SHOULD_PLOT = "shouldPlot"
    SKIP_COLUMNS = "skipColumns"
    ACCURACY_WINDOW = "accuracyWindow"


def evaluateBooleanField(fieldValue):
    return fieldValue is not None and fieldValue == "True"


def readCSV(filePath):
    return np.loadtxt(open(PATH_PREFIX + filePath, "rb"), delimiter=",", dtype=float, skiprows=1)


def readTestsFile(testsFile):
    testsData = None
    with open(testsFile) as file:
        testsData = json.loads(file.read())
    return testsData


def getInputDataAsDicts(testsData):
    dictList = []
    for key, testData in testsData.items():
        for element in testData:
            for value in element.values():
                dictList.append(value)
    # Logging.info("Dict list size: " + str(len(dictList)))
    return dictList


def generateTestsDataFromDict(dataDict):
    actualCsvData = readCSV(dataDict[JsonDataFields.PATH])
    return TestData(actualCsvData, dataDict[JsonDataFields.ALGORITHM_TYPES], dataDict[JsonDataFields.X_NAME], dataDict[JsonDataFields.Y_NAME], dataDict[JsonDataFields.Y_INDEX], dataDict[JsonDataFields.KERNELS], evaluateBooleanField(dataDict[JsonDataFields.SHOULD_PLOT]), dataDict[JsonDataFields.SKIP_COLUMNS], dataDict[JsonDataFields.ACCURACY_WINDOW])


def readAndConvertConfigToDicts(inputFile):
    return getInputDataAsDicts(readTestsFile(inputFile))



