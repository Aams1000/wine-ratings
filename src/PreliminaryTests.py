# Created by Andrew

from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn import preprocessing
from sklearn.svm import SVC
import argparse
import TestDataIO, Kernels, ClassifierEvaluation, Logging, Hyperparameters

SVM_DATA_LIMIT = 10000

# C_VALUES = [1, 0.5, 0.25, 0.1]
C_VALUES = [0.1]

TESTS_FILE_CS221 = "../config/testData221.json"
TESTS_FILE_CS229 = "../config/testData229.json"


class ClassifierName:
    LOGISTIC_REGRESSION = "Logistic Regression"
    LINEAR_REGRESSION = "Linear Regression"
    SVC = "SVC"


class AlgorithmType:
    CLASSIFIER = "Classifier"
    REGRESSION = "Regression"


def logisticRegression(data, shouldPlot):
    Logging.info("Testing LogisticRegression")
    # originalYName = data.yName
    for cValue in C_VALUES:
        # data.yName = originalYName + " (C=" + str(cValue) + ")"
        classifier = LogisticRegression(multi_class="auto", C=cValue)
        data = scaleInputData(data)
        hyperparameters = {Hyperparameters.C : cValue}
        ClassifierEvaluation.trainAndTestClassifier(classifier, ClassifierName.LOGISTIC_REGRESSION, data, shouldPlot, hyperparameters)


def linearRegression(data, shouldPlot):
    Logging.info("Testing LinearRegression")
    classifier = LinearRegression()
    ClassifierEvaluation.trainAndTestClassifier(classifier, ClassifierName.LINEAR_REGRESSION, data, shouldPlot, {})


def supportVectorClassifier(data, kernels, shouldPlot):
    Logging.info("Testing SupportVectorClassifier. Truncating data to first " + str(SVM_DATA_LIMIT) + " values")
    data = truncateTrainingData(data, SVM_DATA_LIMIT)
    data = scaleInputData(data)
    # originalYName = data.yName
    for kernel in kernels:
        Logging.info("Testing " + kernel + " kernel")
        classifier = None
        for cValue in C_VALUES:
            # data.yName = originalYName + " (" + kernel + " kernel, C=" + str(cValue) + ")"
            if kernel == "poly":
                classifier = SVC(gamma="auto", kernel=kernel, degree=3, C=cValue)
            elif kernel == Kernels.RootKernel.NAME:
                classifier = SVC(gamma="auto", kernel=Kernels.RootKernel.rootKernel)
            else:
                classifier = SVC(gamma="auto", kernel=kernel, C=cValue)
            hyperparameters = {Hyperparameters.C : cValue, Hyperparameters.KERNEL : kernel}
            ClassifierEvaluation.trainAndTestClassifier(classifier, ClassifierName.SVC, data, shouldPlot, hyperparameters)


def truncateTrainingData(data, limit):
    data.xTrain = data.xTrain[:limit]
    data.yTrain = data.yTrain[:limit]
    return data


def scaleInputData(data):
    data.xTrain = preprocessing.scale(data.xTrain)
    data.xTest = preprocessing.scale(data.xTest)
    return data


def runClassifiersOnTestData(testData):
    Logging.info("********** ********** ********** ********** ********** ********** ")
    Logging.info("********** TESTING DATA FOR " + testData.xName + " vs. " + testData.yName + " **********")
    Logging.info("********** ********** ********** ********** ********** ********** ")
    dataInfo = ClassifierEvaluation.Data(testData.xTrain, testData.xTest, testData.yTrain, testData.yTest, testData.xName, testData.yName, testData.accuracyWindow)
    for algorithmType in testData.algorithmTypes:
        if algorithmType == AlgorithmType.CLASSIFIER:
            logisticRegression(dataInfo, testData.shouldPlot)
            # SVM truncates data. Making a copy so this doesn't affect models that run after this
            svcDataInfo = ClassifierEvaluation.Data(testData.xTrain, testData.xTest, testData.yTrain, testData.yTest, testData.xName, testData.yName, testData.accuracyWindow)
            supportVectorClassifier(svcDataInfo, testData.kernels, testData.shouldPlot)
        elif algorithmType == AlgorithmType.REGRESSION:
            linearRegression(dataInfo, testData.shouldPlot)


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--cs221", action='store_true', help="Run tests for class CS221")
    parser.add_argument("--cs229", action='store_true', help="Run tests for class CS229")
    args = parser.parse_args()
    return args


def generateTestsDataFromArgs(args):
    testDataDicts = []
    if args.cs221:
        # Logging.info("Adding tests for CS221")
        testDataDicts.extend(TestDataIO.readAndConvertConfigToDicts(TESTS_FILE_CS221))
    if args.cs229:
        # Logging.info("Adding tests for CS229")
        testDataDicts.extend(TestDataIO.readAndConvertConfigToDicts(TESTS_FILE_CS229))
    return testDataDicts


def clearLogs():
    with open(Logging.FILE_NAME, "w") as f:
        f.write("")


def main():
    clearLogs()
    args = parseArgs()
    # Logging.info(args)
    testDataDicts = generateTestsDataFromArgs(args)
    if len(testDataDicts) == 0:
        Logging.info("No tests data found. Please supply an argument to select data. Provided arguments were: ")
        Logging.info(args)

    testData = None
    for dataDict in testDataDicts:
        testData = TestDataIO.generateTestsDataFromDict(dataDict)
        runClassifiersOnTestData(testData)
        testData = None


if __name__ == '__main__':
    main()

