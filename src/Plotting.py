# Created by Andrew

import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plot
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import numpy as np
import Logging

import itertools


PLOT_PREFIX = "../plots/"
PLOT_SUFFIX = ".png"


def plotAndSaveLinear(x, y, predictions, accuracy, info):
    try:
        plot.scatter(x, y, color = 'red')
        plot.plot(x, predictions, color = 'blue')
        plot.title(generateTitle(info.testType, info.classifierName, info.hyperparameters, accuracy))
        plot.xlabel(info.xLabel)
        plot.ylabel(info.yLabel)
        plot.savefig(generateFileName(PLOT_PREFIX, info.destination, PLOT_SUFFIX))
        plot.gcf().clear()
    except:
        Logging.info("Failed to plot " + str(info))
        plot.gcf().clear()


# FIXME - I am broken
def plotAndSaveClassifier(x, y, predictions, accuracy, info):
    try:
        # Logging.info("Inside classifier plot function")
        colors = {}
        dataByLabel = {}
        for index, label in enumerate(y):
            # Logging.info("Got here")
            # Logging.info("Label: " + str(label))
            tupleLabel = np.asscalar(label)
            # Logging.info("Here?")
            if tupleLabel in dataByLabel:
                # Logging.info("Then here")
                dataByLabel[tupleLabel].append(x[index])
                # Logging.info(x[index])
                # Logging.info(x[index].shape)
            # dataByLabel[label][1].append(label)
            else:
                dataByLabel[tupleLabel] = [x[index]]

            # colors[tuple(label)] = [list(np.random.rand(3,))]
        # Logging.info(colors)\
        threeDPlot = None
        fig = None
        if x.shape[1] == 2:
            fig = plot.figure()
            threeDPlot = fig.add_subplot(111, projection='3d')

        for label, inputs in dataByLabel.items():
            npInputs = np.asarray(inputs)
            # Logging.info(npInputs.shape)
            # Logging.info(y.shape)
            npLabels = np.zeros([npInputs.shape[0], ])
            npLabels.fill(label)
            if threeDPlot:
                threeDPlot.scatter(npInputs[:, 0], npInputs[:, 1], npLabels, color=np.random.rand(3,))
            else:
                plot.scatter(npInputs, npLabels, color=np.random.rand(3,))
        # Logging.info(predictions)
        title = generateTitle(info.testType, info.classifierName, info.hyperparameters, accuracy)
        fileName = generateFileName(PLOT_PREFIX, info.destination, PLOT_SUFFIX)
        if threeDPlot:
            threeDPlot.plot(x[:, 0], x[:, 1], predictions, color='blue')
            threeDPlot.set_title(title)
            threeDPlot.set_xlabel(info.xLabel)
            threeDPlot.set_ylabel(info.yLabel)
            threeDPlot.set_zlabel("Predictions")
            fig.savefig(fileName)
        else:
            plot.plot(x, predictions, color='blue')
            plot.title(title)
            plot.xlabel(info.xLabel)
            plot.ylabel(info.yLabel)
            plot.savefig(fileName)
        plot.gcf().clear()
        # Logging.info("Leaving classifier plot function")
    except Exception as e:
        Logging.info("Failed to plot " + str(info))
        Logging.error(e)
        plot.gcf().clear()


#  Copied from https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
def plotConfusionMatrix(cm, classes, normalize, info, accuracy, cmap=plot.cm.Blues):
    plot.figure()
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        Logging.info("Normalized confusion matrix")
    else:
        Logging.info('Confusion matrix, without normalization')

    Logging.info(cm)

    title = generateConfusionMatrixTitle(info.testType, info.classifierName, normalize, info.hyperparameters, accuracy)

    plot.imshow(cm, interpolation='nearest', cmap=cmap)
    plot.title(title)
    plot.colorbar()
    tick_marks = np.arange(len(classes))
    plot.xticks(tick_marks, classes, rotation=45)
    plot.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plot.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plot.ylabel('True label')
    plot.xlabel('Predicted label')
    plot.tight_layout()


    # Compute confusion matrix
    np.set_printoptions(precision=2)
    fileName = generateFileName(PLOT_PREFIX, info.destination + title.replace(" ", "_"), PLOT_SUFFIX)
    plot.savefig(fileName)
    plot.gcf().clear()


def generateTitle(testType, classifierName, hyperparameters, accuracy):
    return testType + " performance for " + classifierName + generateHyperparmeterText(hyperparameters) + " (acc = " + str(accuracy)[:5] + ")"


def generateConfusionMatrixTitle(testType, classifierName, isNormalized, hyperparameters, accuracy):
    normalizedText = " normalized" if isNormalized else ""
    hyperparamText = generateHyperparmeterText(hyperparameters)
    return testType + normalizedText + " CM for " + classifierName + hyperparamText + " (acc = " + str(accuracy)[:5] + ")"


def generateFileName(prefix, destination, suffix):
    return prefix + destination + suffix


def generateHyperparmeterText(hyperparmaters):
    if len(hyperparmaters) == 0:
        return ""
    else:
        hyperparamText = " ("
        for param, value in hyperparmaters.items():
            hyperparamText += str(param) + "=" + str(value) + ", "
    return hyperparamText[:-2] + ")"


class Info(object):

    xLabel = None
    yLabel = None
    destination = None
    testType = None
    classifierName = None
    hyperparameters = None

    # The class "constructor" - It's actually an initializer 
    def __init__(self, classifierName, testType, xName, yName, hyperparamters):
        self.xLabel = xName
        self.yLabel = yName
        self.testType = testType
        self.classifierName = classifierName
        self.hyperparameters = hyperparamters
        self.destination = (classifierName + '_' + xName + '_' + "vs" + '_' + yName + '_' + testType).replace(' ', '_')
